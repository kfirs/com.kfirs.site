## Welcome to the KFIRS project

Hi, I'm Eric Kfir, and you've reached the KFIRS project - my personal
umbrella project for all my personal development. Over the years I have,
as many others in the software development world, developed many useful
pieces of software (some I've developed over and over again). I've
concentrated them here for easy of use and consumption.

### Projects

Currently there are several projects in various stages:

* Maven (com.kfirs.maven)
  This project provides the base POM from which the other projects
  descend, inheriting useful configurations and behaviors.

* Common (com.kfirs.common)
  Provides useful "bite-sized" components useful in most projects.

* Accounting (com.kfirs.accounting)
  Still in development.
