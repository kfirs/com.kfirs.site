## Development overview

Development on the KFIRS project requires:

* [Open JDK](https://openjdk.java.net/install/) version 1.8 and above.
* [Apache Maven](https://maven.apache.org/) version 3.3 and above.
* [IntelliJ](https://www.jetbrains.com/idea) version 2016 and above 
  (though other IDEs can be used).
* Basic understanding of GitFlow is recommended. See
  Vincent Driessen's excellent [GitFlow blog post](http://nvie.com/posts/a-successful-git-branching-model/)
  as well as Atlassian's [GitFlow Tutorial](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)
  for more information.

Please see the [getting started](getting-started.html) page for setting
up your development environment.
