## Branches ##

The main development is done on the `develop` branch, from which 
`release/X.Y.Z` are created. Once those branches are ready to be released
into the world, they are built, deployed to the [Artifactory Repository](http://repo.kfirs.com/), 
the branch is merged back into `master` and `develop`.

To facilitate this workflow, we use [JGitFlow](http://jgitflow.bitbucket.org/) 
for release management.

### Creating a release branch ###

To create a release branch, checkout the `develop` branch, and then
execute this:

    mvn jgitflow:release-start
    
This will ask for the version being prepared (will go in the branch
name and in the POMs inside that branch), and it will also ask for the 
next development version (will go in the POMs inside the `develop`
branch).

It will then:

- create the new branch (`release/X.Y.Z`)
- update the POMs inside it to be the SNAPSHOT of the new version (X.Y.Z)
- update the POMs inside `develop` to the next development version
- switch your working copy to the new release branch

Now you can work on your release branch (usually merging any feature 
branches you've developed) and test it. Once it's ready, you can then
finish the release.

### Finishing a release ###

To finish the release - aka. "release it" - checkout the relevant 
release branch (`release/X.Y.Z`) and then execute this:

    mvn jgitflow:release-finish
    
This will:

- remove the "SNAPSHOT" from the version
- deploy the version to the [Maven repository](http://repo.kfirs.com/)
- merge the branch back into `develop`
- merge the branch back into `master`
- delete the release branch

At this point, the `develop` and `master` branches are fully synced with 
the development in the relevant release branch. The POMs inside the
`develop` branch are still the SNAPSHOT of the next development version
(the one you gave when you created the release branch), and the POMs in
`master` are the released version of your release branch (eg. just 
`X.Y.Z`, without the "-SNAPSHOT" part).
