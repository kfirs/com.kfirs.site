## Getting started

To get started, first ensure you have:

* [Open JDK](https://openjdk.java.net/install/) version 1.8 and above.
* [Apache Maven](https://maven.apache.org/) version 3.3.9 and above.
* Recommended: [IntelliJ](https://www.jetbrains.com/idea) version 2017 and above
  (though other IDEs can be used).

### Apache Maven

All modules in the KFIRS project use Apache Maven. You'll need version
3.3.9 and above. In addition, to access the KFIRS Maven repositories,
you'll need credentials set up for you at the [KFIRS Artifactory Server](https://repo.kfirs.com).
Once you have the credentials, you'll need to add them to your
`settings.xml` file, like this:

    <servers>
        <server>
            <id>remote-repos</id>
            <username>your_username_here</username>
            <password>your_password_here</password>
        </server>
        <server>
            <id>libs-release</id>
            <username>your_username_here</username>
            <password>your_password_here</password>
        </server>
        <server>
            <id>libs-snapshot</id>
            <username>your_username_here</username>
            <password>your_password_here</password>
        </server>
        <server>
            <id>plugins-release</id>
            <username>your_username_here</username>
            <password>your_password_here</password>
        </server>
        <server>
            <id>plugins-snapshot</id>
            <username>your_username_here</username>
            <password>your_password_here</password>
        </server>
    </servers>

You won't ever need to deploy from your machine, since deployment is
handled by the Bitbucket pipelines set up for the KFIRS project.

You'll also need to add `external.atlassian.jgitflow` group ID to
Apache Maven's plugin groups in `settings.xml`, like this:

    <pluginGroups>
        <pluginGroup>external.atlassian.jgitflow</pluginGroup>
        <pluginGroup>org.codehaus.mojo</pluginGroup>
        <pluginGroup>org.springframework.boot</pluginGroup>
    </pluginGroups>

You can use [this settings.xml](settings.xml) file as an example.

### Setting up ###

Once you clone the relevenat Git repositories, you can use standard
Maven commands to build it.
